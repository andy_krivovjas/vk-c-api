#ifndef VKAUDIOOBJECT_H
#define VKAUDIOOBJECT_H

#include <QString>
#include <QVariant>
#include <math.h>

class VkObject {
    public:
        static QString access_token;


};

class VkAudioObject {
public:
    int aid;
    int owner_id;
    QString full_id;
    QString artist;
    QString title;
    QString full_title;
    QString full_title_b;
    int duration;
    QString duration_time;
    QString url;
    QString lyrics_id;
    int genre;

    VkAudioObject();
    VkAudioObject(QVariant item) {
        aid = item.toMap().value("aid").toInt();
        owner_id = item.toMap().value("owner_id").toInt();
        full_id = QString::number(owner_id) + "_" + QString::number(aid);
        artist = item.toMap().value("artist").toString().replace("&amp;", "&");
        title = item.toMap().value("title").toString().replace("&amp;", "&");
        full_title_b = "<b>" + artist + "</b> - " + title;
        full_title = artist + " - " + title;
        duration = item.toMap().value("duration").toInt();
        duration_time = this->convertToTimeString(duration);
        url = item.toMap().value("url").toString().replace("\/", "/").split("?")[0];
        lyrics_id = item.toMap().value("lyrics_id").toString();
        genre = item.toMap().value("genre").toInt();
    }

    QString convertToTimeString(int seconds) {
      QString str;
      int hours, minutes, divisor_for_minutes, divisor_for_seconds;
      // extract hours
      hours = floor(seconds / (60 * 60));

      // extract minutes
      divisor_for_minutes = seconds % (60 * 60);
      minutes = floor(divisor_for_minutes / 60);

      // extract the remaining seconds
      divisor_for_seconds = divisor_for_minutes % 60;
      seconds = ceil(divisor_for_seconds);

      QString Shours, Sminutes, Sseconds;
      Shours = this->checkNumber(QString::number(hours));
      Sminutes = this->checkNumber(QString::number(minutes));
      Sseconds = this->checkNumber(QString::number(seconds));

      str = hours == 0 ? Sminutes + ":" + Sseconds :
                  Shours + ":" + Sminutes + ":" + Sseconds;
      return str;
    }

private:


    QString checkNumber(QString num) {
        return num.length() == 1 ? "0" + num : num;
    }
};

class VkUserObject {
public:
    int user_id;
    QString first_name;
    QString last_name;
    QString full_name;
    QString photo_100;

    VkUserObject();
    VkUserObject(QVariant item) {
        user_id = item.toMap().value("uid").toInt();
        first_name = item.toMap().value("first_name").toString();
        last_name = item.toMap().value("last_name").toString();
        full_name = first_name + " " + last_name;
        photo_100 = item.toMap().value("photo_100").toString();
    }
};

#endif // VKAUDIOOBJECT_H
