#include "settings.h"

QString Settings::access_token = "EMPRY";
QString Settings::user_id = "0";
QString Settings::isBroadcast = "FALSE";
int Settings::volume = 30;

Settings::Settings() {
    redTheme.insert("menuButtonActive", "background-color: rgb(175, 9, 23);");
    redTheme.insert("audioItems", "customLabel[active=true] { background-color: rgb(175, 9, 23); color: #fff;}"
                "customLabel[class_name=list][hover=true] {"
                    "color: #ffffff; "
                    "background-color: #363636;"
                "}"
                "customLabel[hover=true] {"
                    "color: #ffffff;"
                "}"
                "customLabel[active=true][hover=true] {"
                    "background-color: rgb(175, 9, 23);"
                    "color: #fff;"
                "}");
    redTheme.insert("slider", "QSlider::handle:horizontal {background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 rgba(124,50,50,1), stop:1 rgba(220,50,50,1));} #playerContainer{background-color: rgb(90,90,90);border:none;}");

    blueTheme.insert("menuButtonActive", "background-color: #4e7199;");
    blueTheme.insert("audioItems", "customLabel[active=true] { background-color: #4e7199; color: #fff;}"
                "customLabel[class_name=list][hover=true] {"
                    "color: #ffffff; "
                    "background-color: #363636;"
                "}"
                "customLabel[hover=true] {"
                    "color: #ffffff;"
                "}"
                "customLabel[active=true][hover=true] {"
                    "background-color: #4e7199;"
                    "color: #fff;"
                "}");
    blueTheme.insert("slider", "QSlider::handle:horizontal {background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 rgba(44,124,145,0.8), stop:1 rgba(78,183,212,0.8));} #playerContainer{background-color: rgb(90,90,90); border:none;}");

}

void Settings::Load(QString file_name)
{
    QFile f(file_name);
    if(!f.open(QIODevice::ReadOnly)) {
        qDebug() << "error load file";
        return;
    }

    QDataStream stream(&f);
    QVariantMap s;
    stream >> s;
    f.close();

    if(s.contains("access_token")) {
        access_token = s.value("access_token").toString();
        user_id = s.value("user_id").toString();
        isBroadcast = s.value("isBroadcast").toString();
        volume = s.value("volume").toInt();
    }
}

void Settings::Save(QString file_name)
{
    QFile f(file_name);
    if(!f.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        qDebug() << "error save file";
        return;
    }

    QVariantMap params;
    QDataStream stream(&f);

    params.insert("access_token", access_token);
    params.insert("user_id", user_id);
    params.insert("isBroadcast", isBroadcast);
    params.insert("volume", volume);

    stream << params;
    f.close();
}

QVariantMap Settings::LoadTheme(QString ThemeName) {
    if(ThemeName == "red") {
        return redTheme;
    }

    if(ThemeName == "blue") {
        return blueTheme;
    }


    return redTheme;
}
