#include "customlabel.h"

customLabel::customLabel(QWidget *parent) :
    QLabel(parent)
{
}

void customLabel::mouseMoveEvent(QMouseEvent *ev)
{
    this->x = ev->x();
    this->y = ev->y();
    emit Mouse_Pos();
}

void customLabel::mousePressEvent(QMouseEvent *ev)
{
    emit Mouse_Pressed();
}

void customLabel::leaveEvent(QEvent *) {
    emit Mouse_Left();
}

void customLabel::enterEvent(QEvent *) {
    emit Mouse_Hover();
}

void customLabel::addProperty(char* key, char* value)
{
    this->setProperty(key, value);
    this->style()->unpolish(this);
    this->style()->polish(this);
    this->update();
}
