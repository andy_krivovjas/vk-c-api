#ifndef CUSTOMLABEL_H
#define CUSTOMLABEL_H

#include <QLabel>
#include <QMouseEvent>
#include <QEvent>
#include <QStyle>
#include <QDebug>

class customLabel : public QLabel
{
    Q_OBJECT
public:
    explicit customLabel(QWidget *parent = 0);

    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void leaveEvent(QEvent *);
    void enterEvent(QEvent *);

    void addProperty(char* key, char* value);

    int x,y;
signals:
    void Mouse_Pressed();
    void Mouse_Pos();
    void Mouse_Left();
    void Mouse_Hover();

public slots:

};

#endif // CUSTOMLABEL_H
