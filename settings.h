#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QFile>
#include <QDataStream>
#include <QVariant>
#include <QMap>
#include <QDebug>
#include <QVector>

class AssociativeList {
private:
    QString key;
};

class Settings {
public:
    Settings();

    static QString access_token;
    static QString user_id;
    static QString isBroadcast;
    static int volume;

    QVariantMap redTheme;
    QVariantMap blueTheme;

    static void Load(QString file_name = "settings.dat");
    static void Save(QString file_name = "settings.dat");

    QVariantMap LoadTheme(QString ThemeName);
};

#endif // SETTINGS_H
