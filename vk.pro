#-------------------------------------------------
#
# Project created by QtCreator 2014-10-16T15:18:20
#
#-------------------------------------------------

QT       += core gui webkitwidgets webkit network multimediawidgets multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vk
TEMPLATE = app

SOURCES += main.cpp\
        authorize.cpp \
    libs/json.cpp \
    objects/vkobject.cpp \
    settings.cpp \
    libs/customlabel.cpp \
    settingsmodal.cpp

HEADERS  += authorize.h \
    libs/json.h \
    objects/vkobject.h \
    settings.h \
    libs/customlabel.h \
    settingsmodal.h

FORMS    += authorize.ui \
    settingsmodal.ui

OTHER_FILES += \
    main.css

RESOURCES += \
    MyResourses.qrc
