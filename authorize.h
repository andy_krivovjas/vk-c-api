#ifndef AUTHORIZE_H
#define AUTHORIZE_H
#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif

#include <QDebug>
#include <QDialog>
#include <QMainWindow>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QTimer>
#include <QEventLoop>
#include <QVariantList>
#include "libs/json.h"

#include <QtMultimediaWidgets>
#include <QMediaPlayer>
#include <QGraphicsOpacityEffect>

#include <QPropertyAnimation>

#include "objects/vkobject.h"
#include "libs/customlabel.h"
#include "settings.h"
#include "settingsmodal.h"

namespace Ui {
class Authorize;
}


class Authorize : public QMainWindow
{
    Q_OBJECT

public:
    explicit Authorize(QWidget *parent = 0);
    ~Authorize();


private:
    Ui::Authorize *ui;

    QVariantMap theme;

    QMediaPlayer* player;
    QVector<QString> playlist;
    QTimer *playerTimer;
    QVector<VkAudioObject>* audios_global;
    QVector<int> friends_global;

    int currentSongIndex;
    int prevSongIndex;

    bool isPlaying;
    bool isPaused;
    bool isLoop;
    bool isBroadcast;
    bool isPlayerDebug;

    SettingsModal settingsModal;

public slots:
    QByteArray GET(QUrl url, bool isPreloader);
    void check_url(QUrl url);
    void get_audios(int id = -1);
    void get_friends();
    void saerch_audios(QString query, bool self);
    void getPopularAudios();
    void getSimilarAudios();
    void getRecomendations();
    bool IsTokenValid();
    void loginApp();
    void logOutApp();
    VkUserObject* getUserInfo(QString user_id);
    QVariantList getUsersInfo(QString user_ids);

    void buildAudioList(QScrollArea *container, QVariantList audios);
    void buildUsersList(QScrollArea *container, QVariantList users);

    void playTrack(int index);
    void setBroadCast(QString u_aid);
    void playNextTrack();
    void playPrevTrack();
    void pauseTrack();
    void unpauseTrack();
    void updatePlayerPosition();

    void makePlayed(int index);
    void makeHovered(int index);
    void makeLeaved(int index);

private slots:
    void on_popularBtn_pressed();
    void on_myMusicBtn_pressed();
    int on_playpause_clicked();
    void on_playprev_clicked();
    void on_playnext_clicked();
    void animateScroll(QScrollArea *container, int position, int duration);
    void on_search_returnPressed();
    void on_similar_pressed();
    void on_recomendBtn_pressed();
    void on_broadcast_pressed();
    void closeEvent(QCloseEvent *event);
    void on_volumeSlider_valueChanged(int value);
    void on_volumeSlider_sliderPressed();
    void on_volumeSlider_sliderReleased();
    void on_playerPosition_sliderPressed();
    void on_playerPosition_sliderReleased();

    void on_playerPosition_sliderMoved(int position);

    void on_settingsOpen_pressed();
    void on_settingsFormClosed();

    void on_friendsBtn_pressed();

signals:
    void endOfTrack();
};

#endif // AUTHORIZE_H
