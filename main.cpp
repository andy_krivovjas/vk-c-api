#include "authorize.h"
#include <QApplication>
#include <QTextCodec>
#include <QNetworkProxy>

int main(int argc, char *argv[])
{
    QTextCodec *utfcodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(utfcodec);

    QNetworkProxy proxy;
     proxy.setType(QNetworkProxy::Socks5Proxy);
     proxy.setType(QNetworkProxy::HttpProxy);
     proxy.setHostName("10.0.2.1");
     proxy.setPort(8080);
     proxy.setUser("kvv");
     proxy.setPassword("322990");
     //QNetworkProxy::setApplicationProxy(proxy);

     QApplication a(argc, argv);

     QFile file(":/main.css");
     if(file.open(QIODevice::ReadOnly))
     {
       QString data = file.readAll();

       // "this" is the derived QMainWindow class
       a.setStyleSheet(data);
     }


    Settings::Load();
    Authorize w;

    w.move(QApplication::desktop()->screen()->rect().center() - w.rect().center());

    w.show();

    return a.exec();
}
