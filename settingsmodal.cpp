#include "settingsmodal.h"
#include "ui_settingsmodal.h"

SettingsModal::SettingsModal(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsModal)
{
    ui->setupUi(this);


    ui->themeSelect->addItem("Red");
    ui->themeSelect->addItem("Blue");
}

SettingsModal::~SettingsModal()
{
    delete ui;
}

void SettingsModal::on_closeBtn_pressed() {
    emit closed();
    this->close();
}
