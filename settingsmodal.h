#ifndef SETTINGSMODAL_H
#define SETTINGSMODAL_H

#include <QWidget>
#include "settings.h"

namespace Ui {
class SettingsModal;
}

class SettingsModal : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsModal(QWidget *parent = 0);
    ~SettingsModal();

private slots:
    void on_closeBtn_pressed();

private:
    Ui::SettingsModal *ui;

protected:
      void closeEvent() {
          emit closed();
      }
signals:
        void closed(); // closed signal
};

#endif // SETTINGSMODAL_H
