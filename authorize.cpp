#include "authorize.h"
#include "ui_authorize.h"

using namespace QtJson;

Authorize::Authorize(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Authorize)
{
    ui->setupUi(this);

    Authorize::player = new QMediaPlayer;
    Authorize::playerTimer = new QTimer(this);
    Authorize::audios_global = new QVector<VkAudioObject>();
    Authorize::currentSongIndex = 0;
    Authorize::prevSongIndex = 0;

    Authorize::isPlaying = FALSE;
    Authorize::isPaused = FALSE;
    Authorize::isLoop = FALSE;
    Authorize::isBroadcast = FALSE;
    Authorize::isPlayerDebug = FALSE;

    Settings* setting = new Settings();
    theme = setting->LoadTheme("red");

    //Loading settings
    if(Settings::isBroadcast == "TRUE") {
        isBroadcast =  TRUE;
        ui->broadcast->setStyleSheet("border-image: url(:/images/broadcast-on.png) 0 0 0 0 stretch stretch; background: none;");
    } else {
        isBroadcast =  FALSE;
        ui->broadcast->setStyleSheet("border-image: url(:/images/broadcast-off.png) 0 0 0 0 stretch stretch; background: none;");
    }
    ui->playerContainer->setStyleSheet(theme.value("slider").toString());

    ui->volumeSlider->setValue(Settings::volume);
    player->setVolume(Settings::volume);

    ui->textDisplay->hide();

    //connect(settingsModal, SIGNAL(closed()), this, SLOT(on_settingsFormClosed()));

    if(IsTokenValid()) {
        loginApp();
    } else {
        logOutApp();

        QUrlQuery url = QUrlQuery("https://oauth.vk.com/authorize?");
            url.addQueryItem("client_id", "4586798");
            url.addQueryItem("scope", "audio,friends,offline,wall,status,messages");
            url.addQueryItem("display", "popup");
            url.addQueryItem("response_type", "token");

        ui->web_login->load(QUrl(url.toString()));
    }

    connect(ui->web_login, SIGNAL(urlChanged(QUrl)),this,SLOT(check_url(QUrl)));


}

Authorize::~Authorize()
{
    delete ui;
}

QByteArray Authorize::GET(QUrl url, bool isPreloader) {

    if(isPreloader)
     ui->preloader->show();

    QNetworkAccessManager* manager = new QNetworkAccessManager(this);
    QNetworkReply* reply = manager->get(QNetworkRequest(url));
    QEventLoop wait;
    connect(manager, SIGNAL(finished(QNetworkReply*)), &wait, SLOT(quit()));
    QTimer::singleShot(10000, &wait, SLOT(quit()));
    wait.exec();

    QByteArray response = reply->readAll();
    reply->deleteLater();
    connect(manager, SIGNAL(finished(QNetworkReply*)), manager, SLOT(deleteLater()));

    if(isPreloader)
        ui->preloader->hide();

    return response;
}

void Authorize::check_url(QUrl url) {

    if(!url.toString().contains("authorize")) {

        url = url.toString().replace("#", "?");
        QString token = QUrlQuery(url).queryItemValue("access_token");
        QString user = QUrlQuery(url).queryItemValue("user_id");

        Settings::access_token = token;
        Settings::user_id = user;
        Settings::Save();
        loginApp();
    }
}

void Authorize::get_audios(int id) {

    QUrlQuery current = QUrlQuery("https://api.vk.com/method/audio.get?");
        if(id == -1) {
            current.addQueryItem("owner_id", Settings::user_id);
        } else {
            current.addQueryItem("owner_id", QString::number(friends_global[id]));
        }

        current.addQueryItem("access_token", Settings::access_token);

    QByteArray response = GET(QUrl(current.toString()), TRUE);

    QVariantList audios = parse(response).toMap().value("response").toList();

    buildAudioList(ui->myMusic, audios);

}

void Authorize::get_friends() {

    QUrlQuery current = QUrlQuery("https://api.vk.com/method/friends.get?");
        current.addQueryItem("user_id", Settings::user_id);
        current.addQueryItem("order", "hints");
        current.addQueryItem("access_token", Settings::access_token);

    QByteArray response = GET(QUrl(current.toString()), TRUE);

    QVariantList friends = parse(response).toMap().value("response").toList();

    QString ids = "";
    for(int i = 0; i < friends.size(); i++) {
        ids += friends[i].toString();
        if(i != friends.size() - 1) {
            ids += ",";
        }
    }

    friends = getUsersInfo(ids);

    buildUsersList(ui->myMusic, friends);

}

void Authorize::saerch_audios(QString query, bool self) {
    QUrlQuery current = QUrlQuery("https://api.vk.com/method/audio.search?");
        current.addQueryItem("q", query);
        current.addQueryItem("auto_complete", "1");
        current.addQueryItem("count", "1000");
        if(self) current.addQueryItem("search_own", "1");
        current.addQueryItem("access_token", Settings::access_token);


    QByteArray response = GET(QUrl(current.toString()), TRUE);
    QVariantList audios = parse(response).toMap().value("response").toList();

    buildAudioList(ui->myMusic, audios);
}

void Authorize::getPopularAudios() {
    QUrlQuery current = QUrlQuery("https://api.vk.com/method/audio.getPopular?");
        current.addQueryItem("auto_complete", "1");
        current.addQueryItem("count", "1000");
        current.addQueryItem("access_token", Settings::access_token);

    QByteArray response = GET(QUrl(current.toString()), TRUE);

    QVariantList audios = parse(response).toMap().value("response").toList();

    buildAudioList(ui->myMusic, audios);
}

void Authorize::getSimilarAudios() {
    QUrlQuery current = QUrlQuery("https://api.vk.com/method/audio.getRecommendations?");
        current.addQueryItem("target_audio", audios_global->at(currentSongIndex).full_id);
        current.addQueryItem("count", "1000");
        current.addQueryItem("access_token", Settings::access_token);

    QByteArray response = GET(QUrl(current.toString()), TRUE);

    QVariantList audios = parse(response).toMap().value("response").toList();

    buildAudioList(ui->myMusic, audios);
}

void Authorize::getRecomendations() {
    QUrlQuery current = QUrlQuery("https://api.vk.com/method/audio.getRecommendations?");
        current.addQueryItem("count", "1000");
        current.addQueryItem("access_token", Settings::access_token);

    QByteArray response = GET(QUrl(current.toString()), TRUE);

    QVariantList audios = parse(response).toMap().value("response").toList();

    buildAudioList(ui->myMusic, audios);
}

bool Authorize::IsTokenValid() {

    QUrlQuery current = QUrlQuery("https://api.vk.com/method/audio.get?");
        current.addQueryItem("owner_id", Settings::user_id);
        current.addQueryItem("access_token", Settings::access_token);

    QByteArray response = GET(QUrl(current.toString()), FALSE);

    if(response.contains("response"))
        return true;
    return false;
}

void Authorize::loginApp() {

    ui->web_login->load(QUrl("authorize"));
    ui->web_login->hide();
    ui->frame->show();
    VkUserObject* me = getUserInfo(Settings::user_id);

    ui->first_name->setText(me->first_name);
    ui->last_name->setText(me->last_name);
    QWidget::adjustSize();
    ui->avatar->load(QUrl(me->photo_100));

    ui->preloader->hide();

    ui->myMusicBtn->setStyleSheet(theme.value("menuButtonActive").toString());
    ui->myMusic->setStyleSheet(theme.value("audioItems").toString());
    get_audios();
}

void Authorize::logOutApp() {
    Settings::access_token = "";
    player->stop();
    this->setWindowTitle("VkPlayer");

    ui->web_login->show();
    ui->frame->hide();
}

VkUserObject* Authorize::getUserInfo(QString user_id) {

    QUrlQuery current = QUrlQuery("https://api.vk.com/method/users.get?");
        current.addQueryItem("user_ids", user_id);
        current.addQueryItem("fields", "photo_100");
        current.addQueryItem("access_token", Settings::access_token);

    QByteArray response = GET(QUrl(current.toString()), FALSE);

    QVariantList user = parse(response).toMap().value("response").toList();

    return new VkUserObject(user[0]);
}

QVariantList Authorize::getUsersInfo(QString user_ids) {

    QUrlQuery current = QUrlQuery("https://api.vk.com/method/users.get?");
        current.addQueryItem("user_ids", user_ids);
        current.addQueryItem("fields", "photo_100");
        current.addQueryItem("access_token", Settings::access_token);

    QByteArray response = GET(QUrl(current.toString()), FALSE);

    QVariantList users = parse(response).toMap().value("response").toList();

    return users;
}

void Authorize::buildAudioList(QScrollArea *container, QVariantList audios) {

    playlist.clear();
    audios_global->clear();

    int length = audios.size();
    VkAudioObject* audio_list[1000];

    QWidget* wrapper = new QWidget(container);
    QVBoxLayout* flow = new QVBoxLayout(wrapper);
    flow->setContentsMargins(0,0,0,0);
    flow->setMargin(0);
    flow->setSpacing(0);

    QSignalMapper* signalMapper = new QSignalMapper(this);
    QSignalMapper* signal_hover = new QSignalMapper(this);
    QSignalMapper* signal_leave = new QSignalMapper(this);

    int i;
    for(i = 1; i < audios.size(); i++) {
        audio_list[i] = new VkAudioObject(audios[i]);
        audios_global->append(audios[i]);
        customLabel* label = new customLabel(wrapper);

        label->setToolTip(audio_list[i]->full_title_b);
        label->setCursor(Qt::PointingHandCursor);
        label->setObjectName("lbl" + QString::number(i));
        label->addProperty("class_name", "list");

        QHBoxLayout* label_layout = new QHBoxLayout(label);
        label_layout->setContentsMargins(0,0,0,0);
        label_layout->setMargin(0);
        label_layout->setSpacing(0);

        customLabel* title = new customLabel(label);
        customLabel* duration = new customLabel(label);

        title->setText(audio_list[i]->full_title_b);
        title->setObjectName("title" + QString::number(i));
        duration->setText(audio_list[i]->duration_time);
        duration->setObjectName("duration" + QString::number(i));
        duration->addProperty("class_name", "duration");

        label_layout->addWidget(title);
        label_layout->addWidget(duration);
        label->setLayout(label_layout);

        playlist.append(audio_list[i]->url);
        signalMapper->setMapping(label, i - 1);

        signal_hover->setMapping(label, i - 1);
        signal_leave->setMapping(label, i - 1);
        connect(label, SIGNAL(Mouse_Pressed()),signalMapper, SLOT (map()));

        signalMapper->setMapping(title, i - 1);
        connect(title, SIGNAL(Mouse_Pressed()),signalMapper, SLOT (map()));
        signalMapper->setMapping(duration, i - 1);
        connect(duration, SIGNAL(Mouse_Pressed()),signalMapper, SLOT (map()));

        connect(label, SIGNAL(Mouse_Hover()),signal_hover, SLOT (map()));
        connect(label, SIGNAL(Mouse_Left()),signal_leave, SLOT (map()));

        flow->addWidget(label);
    }

    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(playTrack(int)));
    connect(signal_hover, SIGNAL(mapped(int)), this, SLOT(makeHovered(int)));
    connect(signal_leave, SIGNAL(mapped(int)), this, SLOT(makeLeaved(int)));

    connect(this, SIGNAL(endOfTrack()), this, SLOT(playNextTrack()));

    wrapper->setLayout(flow);
    container->setWidget(wrapper);
    container->setWidgetResizable(true);
}

void Authorize::buildUsersList(QScrollArea *container, QVariantList users) {

    friends_global.clear();

    VkUserObject* users_list[users.size()];

    QWidget* wrapper = new QWidget(container);
    QVBoxLayout* flow = new QVBoxLayout(wrapper);
    flow->setContentsMargins(0,0,0,0);
    flow->setMargin(0);
    flow->setSpacing(0);

    QSignalMapper* signalMapper = new QSignalMapper(this);
    QHBoxLayout* label_layout;

    int i;
    for(i = 0; i < users.size(); i++) {

        users_list[i] = new VkUserObject(users[i]);
        friends_global.append(users_list[i]->user_id);
        customLabel* label = new customLabel(wrapper);

        label->setCursor(Qt::PointingHandCursor);
        label->setObjectName("lbl" + QString::number(i));
        label->addProperty("class_name", "list-of-users");

        label_layout = new QHBoxLayout(label);
        label_layout->setContentsMargins(0,0,0,0);
        label_layout->setMargin(0);
        label_layout->setSpacing(0);

        customLabel* title = new customLabel(label);
        QWebView* avatar = new QWebView(label);

        title->setText(users_list[i]->full_name);
        title->setObjectName("title" + QString::number(i));
        title->addProperty("class_name", "username");
        title->setAlignment(Qt::AlignTop);
        avatar->load(QUrl(users_list[i]->photo_100));
        avatar->setEnabled(false);
        avatar->setMaximumWidth(70);
        avatar->setMaximumHeight(70);
        avatar->setToolTip(users_list[i]->full_name);

        label_layout->addWidget(avatar);
        label_layout->addWidget(title);
        label->setLayout(label_layout);

        signalMapper->setMapping(label, i);

        connect(label, SIGNAL(Mouse_Pressed()),signalMapper, SLOT (map()));

        flow->addWidget(label);
    }

    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(get_audios(int)));

    wrapper->setLayout(flow);
    container->setWidget(wrapper);
    container->setWidgetResizable(true);
}


void Authorize::playTrack(int index) {

    player->stop();
    player->setMedia(QMediaContent(QUrl(playlist[index])));

    qDebug() << playlist[index];
    //player->setVolume(50);
    player->play();

    makePlayed(index);
    if(isBroadcast)
        setBroadCast(audios_global->at(index).full_id);

    //ui->myMusic->verticalScrollBar()->setValue(currentSongIndex * 34);

    connect(playerTimer, SIGNAL(timeout()), this, SLOT(updatePlayerPosition()));
    playerTimer->start(100);

}

void Authorize::setBroadCast(QString u_aid) {
    QUrlQuery current = QUrlQuery("https://api.vk.com/method/audio.setBroadcast?");
        current.addQueryItem("audio", u_aid);
        current.addQueryItem("access_token", Settings::access_token);
    QByteArray response = GET(QUrl(current.toString()), FALSE);

    qDebug() << response;
}

void Authorize::playNextTrack() {
    playerTimer->stop();
    playTrack(currentSongIndex + 1);
}

void Authorize::playPrevTrack() {
    playerTimer->stop();
    playTrack(currentSongIndex - 1);
}

void Authorize::pauseTrack() {
    player->pause();
    isPaused = TRUE;
    isPlaying = FALSE;
    ui->playpause->setStyleSheet("");
    this->setWindowTitle("*Paused* " + audios_global->at(currentSongIndex).full_title);
}

void Authorize::unpauseTrack() {
    player->play();
    isPaused = FALSE;
    isPlaying = TRUE;
    ui->playpause->setStyleSheet("border-image: url(:/images/pause.png) 0 0 0 0 stretch stretch;");
    this->setWindowTitle("*Now Playing* " + audios_global->at(currentSongIndex).full_title);
}

void Authorize::updatePlayerPosition()
{
    int currentPercent = ui->playerPosition->maximum() * player->position() / player->duration();
    if(isPlayerDebug) {
        qDebug() << player->mediaStatus();
        qDebug() << "Duration: " + QString::number(player->duration());
        qDebug() << "position: " + QString::number(player->position());
        qDebug() << "currentPercent: " + QString::number(currentPercent) + " of " + QString::number(ui->playerPosition->maximum());
    }

    ui->playerPosition->setValue(currentPercent);
    VkAudioObject* a;
    ui->duration->setText(a->convertToTimeString(player->position() / 1000));
    ui->duration_time->setText(a->convertToTimeString(player->duration() / 1000));

    if(currentPercent >= ui->playerPosition->maximum()) {
        emit endOfTrack();
    }
}

void Authorize::makePlayed(int index) {

    isPlaying = TRUE;

   // qDebug() << prevSongIndex;
    //qDebug() << currentSongIndex;

    prevSongIndex = currentSongIndex;
    currentSongIndex = index;

    ui->playpause->setStyleSheet("border-image: url(:/images/pause.png) 0 0 0 0 stretch stretch;");
    this->setWindowTitle("*Now Playing* " + audios_global->at(index).full_title);


    if(currentSongIndex == 0) {
        prevSongIndex = playlist.size() - 1;
    }
    if(currentSongIndex == playlist.size() - 1) {
        prevSongIndex = 0;
    }

    ui->myMusic->findChild<customLabel*>("lbl" + QString::number(prevSongIndex + 1))->addProperty("active", "false");
    ui->myMusic->findChild<customLabel*>("title" + QString::number(prevSongIndex + 1))->addProperty("active", "false");
    ui->myMusic->findChild<customLabel*>("duration" + QString::number(prevSongIndex + 1))->addProperty("active", "false");

    ui->myMusic->findChild<customLabel*>("lbl" + QString::number(index + 1))->addProperty("active", "true");
    ui->myMusic->findChild<customLabel*>("title" + QString::number(index + 1))->addProperty("active", "true");
    ui->myMusic->findChild<customLabel*>("duration" + QString::number(index + 1))->addProperty("active", "true");
}

void Authorize::makeHovered(int index) {
    ui->myMusic->findChild<customLabel*>("lbl" + QString::number(index + 1))->addProperty("hover", "true");
    ui->myMusic->findChild<customLabel*>("title" + QString::number(index + 1))->addProperty("hover", "true");
    ui->myMusic->findChild<customLabel*>("duration" + QString::number(index + 1))->addProperty("hover", "true");
}

void Authorize::makeLeaved(int index) {
    ui->myMusic->findChild<customLabel*>("lbl" + QString::number(index + 1))->addProperty("hover", "false");
    ui->myMusic->findChild<customLabel*>("title" + QString::number(index + 1))->addProperty("hover", "false");
    ui->myMusic->findChild<customLabel*>("duration" + QString::number(index + 1))->addProperty("hover", "false");
}

void Authorize::on_popularBtn_pressed() {
    ui->myMusicBtn->setStyleSheet("");
    ui->recomendBtn->setStyleSheet("");
    ui->friendsBtn->setStyleSheet("");
    ui->popularBtn->setStyleSheet(theme.value("menuButtonActive").toString());
    getPopularAudios();
}

void Authorize::on_myMusicBtn_pressed() {
    ui->popularBtn->setStyleSheet("");
    ui->recomendBtn->setStyleSheet("");
    ui->friendsBtn->setStyleSheet("");
    ui->myMusicBtn->setStyleSheet(theme.value("menuButtonActive").toString());
    get_audios();
}


void Authorize::on_recomendBtn_pressed() {
    ui->myMusicBtn->setStyleSheet("");
    ui->recomendBtn->setStyleSheet(theme.value("menuButtonActive").toString());
    ui->popularBtn->setStyleSheet("");
    ui->friendsBtn->setStyleSheet("");
    getRecomendations();
}

void Authorize::on_friendsBtn_pressed() {
    ui->popularBtn->setStyleSheet("");
    ui->recomendBtn->setStyleSheet("");
    ui->myMusicBtn->setStyleSheet("");
    ui->friendsBtn->setStyleSheet(theme.value("menuButtonActive").toString());
    get_friends();
}

int Authorize::on_playpause_clicked() {
    if(!isPlaying && !isPaused) {
        playTrack(currentSongIndex);
    } else if(!isPlaying && isPaused) {
        unpauseTrack();
    } else if(isPlaying && !isPaused) {
        pauseTrack();
    }

    return 0;
}

void Authorize::on_playprev_clicked() {
    if(currentSongIndex == 0) {
        currentSongIndex = playlist.size();
    }

    playPrevTrack();
}

void Authorize::on_playnext_clicked() {
    if(currentSongIndex == playlist.size() - 1) {
        currentSongIndex = -1;
    }

    playNextTrack();
}

void Authorize::animateScroll(QScrollArea *container, int position, int duration) {
    int currentPosition = container->verticalScrollBar()->value();
    int diff = abs(currentPosition - position);
    int speed = diff / duration;

    container->verticalScrollBar()->setValue(position);
    if(currentPosition < position) {
        for(int i = currentPosition; i<position; i++) {
            //container->verticalScrollBar()->setValue(i);
        }
    }

}

void Authorize::on_search_returnPressed() {
    this->saerch_audios(ui->search->text(), TRUE);
}

void Authorize::on_similar_pressed() {
    getSimilarAudios();
}

void Authorize::on_broadcast_pressed() {
    if(isBroadcast) {
        isBroadcast = FALSE;
        Settings::isBroadcast = "FALSE";
        ui->broadcast->setStyleSheet("border-image: url(:/images/broadcast-off.png) 0 0 0 0 stretch stretch; background: none;");
        setBroadCast("");
    } else {
        isBroadcast = TRUE;
        Settings::isBroadcast = "TRUE";
        ui->broadcast->setStyleSheet("border-image: url(:/images/broadcast-on.png) 0 0 0 0 stretch stretch; background: none;");
    }
}

void Authorize::closeEvent(QCloseEvent *event) {
    Settings::Save();
    event->accept();

}

void Authorize::on_volumeSlider_valueChanged(int value) {
    player->setVolume(value);
    Settings::volume = value;
    ui->textDisplay->setText(QString::number(value));
}

void Authorize::on_volumeSlider_sliderPressed() {
    ui->textDisplay->setText(QString::number(ui->volumeSlider->value()));
    ui->textDisplay->show();
}

void Authorize::on_volumeSlider_sliderReleased() {
    ui->textDisplay->hide();
}

void Authorize::on_playerPosition_sliderPressed() {
    ui->textDisplay->setText(QString::number(ui->volumeSlider->value()));
    ui->textDisplay->show();
}

void Authorize::on_playerPosition_sliderReleased() {
    ui->textDisplay->hide();
}

void Authorize::on_playerPosition_sliderMoved(int position) {
    playerTimer->stop();
    qint64 pos = player->duration() * position / ui->playerPosition->maximum();
    player->setPosition(pos);
    playerTimer->start(100);

    VkAudioObject* a;
    ui->textDisplay->setText(a->convertToTimeString(pos / 1000));
}

void Authorize::on_settingsOpen_pressed() {
    //settingsModal.move(QApplication::desktop()->screen()->rect().center() - settingsModal.rect().center());
    //settingsModal.show();


    //this->setEnabled(false);


    ui->web_login->settings()->clearMemoryCaches();
    ui->web_login->page()->networkAccessManager()->setCookieJar(new QNetworkCookieJar());
    QWebSettings::clearMemoryCaches();

    QUrlQuery url = QUrlQuery("https://oauth.vk.com/authorize?");
        url.addQueryItem("client_id", "4586798");
        url.addQueryItem("scope", "audio,friends,offline,wall,status,messages");
        url.addQueryItem("display", "popup");
        url.addQueryItem("response_type", "token");

    ui->web_login->load(QUrl(url.toString()));
    logOutApp();
    //connect(ui->web_login, SIGNAL(urlChanged(QUrl)),this,SLOT(check_url(QUrl)));
}

void Authorize::on_settingsFormClosed() {
   this->setEnabled(true);
}


